/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * The DFL::Clipboard::Manager class manages the wayland clipboard.
 *
 * IMPL Class
 *  This class will read/write the different clipboard database files
 *  This is purely an implementational detail.
 *  This class can be modified/removed at anytime.
 **/

#include <algorithm>

#include "Table.hpp"

#include <wayqt/DataControl.hpp>

namespace DFLClip = DFL::Clipboard;

DFLClip::Table::Table( QString path, DFLClip::Type type ) {
    mType = type;

    if ( not path.endsWith( "/" ) ) {
        path += "/";
    }

    dbf = std::make_unique<QSettings>(
        std::move( path + ( type == Clipboard ? "Clipboard.ini" : "Selection.ini" ) ),
        QSettings::IniFormat
    );

    /** Get the date-time of the entries */
    QStringList dateTimes = dbf->childGroups();

    /** Sort them in ascending order */
    dateTimes.sort();

    /** Reverse the list: we need the latest date-time first */
    std::reverse( dateTimes.begin(), dateTimes.end() );

    /** Read the entries */
    for (const QString& dateTime : dateTimes) {
        dbf->beginGroup( dateTime );

        if ( !dbf->allKeys().isEmpty() ) {
            auto entry = QSharedPointer<DFL::Clipboard::Entry>::create( dateTime );
            for (const QString& format : dbf->allKeys() ) {
                QByteArray hexData = dbf->value( format ).toByteArray();
                entry->setData( format, QByteArray::fromHex( hexData ) );
            }
            mEntries.push_back( std::move( entry ) );
        }

        dbf->endGroup();
    }
}


DFLClip::Type DFLClip::Table::type() {
    return mType;
}


void DFLClip::Table::clear() {
    dbf->clear();
    mEntries.clear();
}


void DFLClip::Table::addEntry( WQt::MimeData mimeData ) {
    /**
     * Add new entries only if it does not already exist.
     * The user may do one or more of the following:
     *  1. Press Ctrl-C repeatedly.
     *  2. Copy a text that was previously copied
     *  3. The new entry is a subset of previous entry
     *  4. The previous entry is a subset of new entry
     * The last two cases are not handled currently. <== TODO
     *
     * To handle the first and the second cases, we check
     * if the entry already exists. If it doesn, then we
     * can simply promote that entry to the top.
     */

    auto newEntry = QSharedPointer<DFL::Clipboard::Entry>::create( QDateTime::currentDateTime(), mimeData );

    // Find existing entry
    auto it = std::find_if(
        mEntries.begin(), mEntries.end(), [ &newEntry ](const auto& existingEntry) {
            return *existingEntry == *newEntry;
        }
    );

    if ( it != mEntries.end() ) {
        promoteToTop( std::distance( mEntries.begin(), it ) );
        return;
    }

    // Add new entry
    mEntries.push_front( std::move( newEntry ) );

    // Trim history
    while ( mHistorySize < mEntries.size() ) {
        mEntries.pop_back();
    }

    storeEntries();
}


void DFLClip::Table::removeEntry( int index ) {
    mEntries.erase( mEntries.begin() + index );
    storeEntries();
}


void DFLClip::Table::replaceEntry( int index, WQt::MimeData mimeData ) {
    // Validate index bounds
    if ( ( index < 0 ) || ( index >= mEntries.size() ) ) {
        return; // Or throw an out_of_range exception
    }

    // Create new entry with original entry's datetime
    auto newEntry = QSharedPointer<DFL::Clipboard::Entry>::create( mEntries[ index ]->dateTime(), mimeData );

    // Replace entry using move semantics
    mEntries[ index ] = std::move( newEntry );

    // Rewrite the db file
    storeEntries();
}


void DFLClip::Table::promoteToTop( int index ) {
    // Validate index bounds
    if ( ( index <= 0 ) || ( index >= mEntries.size() ) ) {
        return; // Or throw an out_of_range exception
    }

    // Take and move the entry, using unique_ptr semantics
    auto entry = std::move( mEntries[ index ] );

    // Refresh the entry's datetime
    entry->refresh();

    // Remove the original entry
    mEntries.erase( mEntries.begin() + index );

    // Insert at the beginning
    mEntries.insert( mEntries.begin(), std::move( entry ) );

    // Rewrite the db file
    storeEntries();
}


int DFLClip::Table::count() {
    return mEntries.count();
}


DFL::Clipboard::Entry * DFLClip::Table::entry( int idx ) {
    return mEntries[ idx ].get();
}


void DFLClip::Table::storeEntries() {
    // Early return if history storage is disabled
    if ( !mStoreHistory ) {
        return;
    }

    // Reset the database
    dbf->clear();

    // Write each entry as a dedicated section
    for (const auto& entry : mEntries) {
        // Use const reference to avoid unnecessary copying
        dbf->beginGroup( entry->dateTime().toString( "yyyyMMdd-hhmmss.zzz" ) );

        for (const QString& fmt : entry->formats() ) {
            // Hex encoding of data for storage
            dbf->setValue( fmt, entry->data( fmt ).toHex() );
        }

        dbf->endGroup();
    }

    // Ensure it's written to the disk
    dbf->sync();
}


void DFLClip::Table::setStoreHistory( bool yes ) {
    mStoreHistory = yes;

    if ( not yes ) {
        dbf->clear();
    }
}


void DFLClip::Table::setHistorySize( quint64 size ) {
    mHistorySize = size;

    /** Remove the outdated entries from the bottom */
    while ( mHistorySize < mEntries.count() ) {
        mEntries.removeLast();
    }
}


/**
 * Entry
 */

DFL::Clipboard::Entry::Entry() {
    /** Do nothing here */
}


DFL::Clipboard::Entry::Entry( QString strDT ) {
    /** Set the date-time */
    mDateTime = QDateTime::fromString( strDT, "yyyyMMdd-hhmmss.zzz" );
}


DFL::Clipboard::Entry::Entry( QDateTime dt, WQt::MimeData mimeData ) {
    /** Store the date-time */
    mDateTime = dt;

    /** Store the data */
    mMimeData = mimeData;
}


DFL::Clipboard::Entry::Entry( const DFL::Clipboard::Entry& other ) {
    /** Set the date-time */
    mDateTime = other.mDateTime;

    /** Copy the data from @other */
    mMimeData = other.mMimeData;
}


bool DFL::Clipboard::Entry::isValid() {
    return mDateTime.isValid();
}


QDateTime DFL::Clipboard::Entry::dateTime() {
    return mDateTime;
}


QStringList DFL::Clipboard::Entry::formats() {
    return mMimeData.formats();
}


QByteArray DFL::Clipboard::Entry::data( QString format ) {
    return mMimeData.data( format );
}


void DFL::Clipboard::Entry::setData( QString format, QByteArray data ) {
    mMimeData.setData( format, data );
}


void DFL::Clipboard::Entry::clear() {
    mMimeData.clear();
}


void DFL::Clipboard::Entry::refresh() {
    mDateTime = QDateTime::currentDateTime();
}


DFL::Clipboard::Entry& DFL::Clipboard::Entry::operator=( const DFL::Clipboard::Entry& other ) {
    /** Copy the time and data from @other */

    mDateTime = other.mDateTime;
    mMimeData = other.mMimeData;
    return *this;
}


bool DFL::Clipboard::Entry::operator==( const DFL::Clipboard::Entry& other ) const {
    return ( mMimeData == other.mMimeData );
}


WQt::MimeData DFL::Clipboard::Entry::data() {
    return mMimeData;
}


WQt::MimeData DFL::Clipboard::Entry::data() const {
    return mMimeData;
}
