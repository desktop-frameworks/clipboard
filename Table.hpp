/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * The DFL::Clipboard::Manager class manages the wayland clipboard.
 *
 * IMPL Class
 *  This class will read/write the different clipboard database files
 *  This is purely an implementational detail.
 *  This class can be modified/removed at anytime.
 **/

#pragma once

#include <QtCore>
#include "DFClipboardManager.hpp"
#include <wayqt/DataControl.hpp>

namespace DFL {
    namespace Clipboard {
        class Table;
        class Entry;

        typedef QVector<QSharedPointer<DFL::Clipboard::Entry> > Entries;
    }
}

class DFL::Clipboard::Table {
    public:
        Table( QString path, DFL::Clipboard::Type type );

        /**
         * Returns the table type.
         * Clipboard - This is the classic Ctrl-C/Ctrl-X clipboard.
         * Selection - Clipboard accessible via mouse selection.
         */
        DFL::Clipboard::Type type();

        /**
         * Clear the table. This will cause the corresponding
         * clipboard to be set to null, and the database file
         * emptied. Cannot be undone.
         */
        void clear();

        /**
         * Add a new entry.
         * This will be added only if an entry
         * of this index does not exist in the
         * database.
         */
        void addEntry( WQt::MimeData entry );

        /**
         * Remove the entry corresponding to
         * the index @idx;
         */
        void removeEntry( int idx );

        /**
         * Replace the entry at index @idx;
         */
        void replaceEntry( int idx, WQt::MimeData entry );

        /**
         * Push the entry at @idx to first position
         */
        void promoteToTop( int idx );

        /**
         * Retrieve the entry at @idx
         */
        int count();

        /**
         * Retrieve the entry at @idx
         */
        DFL::Clipboard::Entry *entry( int idx );

        /**
         * Save the entries in @mEntries into @dbf;
         */
        void storeEntries();

        /**
         * Store the history in a file
         */
        void setStoreHistory( bool yes );
        void setHistorySize( quint64 size );

    private:
        std::unique_ptr<QSettings> dbf;

        Entries mEntries;
        DFL::Clipboard::Type mType;
        bool mStoreHistory = false;
        int mHistorySize   = 10;
};


class DFL::Clipboard::Entry {
    public:

        /**
         * Because this is a must for use with QList<>.
         */
        Entry();

        /**
         * Useful for recreating an entry stored in the database
         * The parameter passed is the stringified date-time or QDateTime
         * Format: yyyyMMdd-hhmmss.zzz
         */
        Entry( QString );

        /**
         * Useful for creating a new entry for a given MimeData
         * Date time will be initialized to current date-time
         */
        Entry( QDateTime, WQt::MimeData );

        /**
         * Useful while copying an Entry object.
         * We will inherit the date time of the other Entry.
         */
        Entry( const DFL::Clipboard::Entry& );

        /**
         * Is this a valid entry?
         * Valid as long as we have a valid date.
         */
        bool isValid();

        /**
         * Get the date-time when this data was created/last offered
         */
        QDateTime dateTime();

        /**
         * Obtain the list of all stored formats.
         * The returned list will be in ascedning order.
         */
        QStringList formats();

        /**
         * Retrieve data corresponding to the given format.
         * If the format does not exist, an empty QByteArray is returned.
         */
        QByteArray data( QString );

        /**
         * Store the format, and the corresponding data
         * Setting an empty data to a format removes it.
         */
        void setData( QString format, QByteArray data );

        /**
         * Clear all the formats stored
         */
        void clear();

        /**
         * Update the date-time to current date time
         */
        void refresh();

        /**
         * The assignment operator
         */
        DFL::Clipboard::Entry& operator=( const DFL::Clipboard::Entry& );

        /**
         * The equality operator
         */
        bool operator==( const DFL::Clipboard::Entry& ) const;

        /**
         * Simple way to get the underlying MimeData object
         */
        WQt::MimeData data();
        WQt::MimeData data() const;

    private:
        WQt::MimeData mMimeData;
        QDateTime mDateTime;
};
