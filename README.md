# DFL::Clipboard

This is a simple clipboard manager built using the wlroots wlr-data-control protocol.


### Dependencies:
* <tt>Qt5 (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>[WayQt](https://gitlab.com/desktop-frameworks/wayqt.git</tt>


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/clipboard.git dfl-clipboard`
- Enter the `dfl-clipboard` folder
  * `cd dfl-clipboard`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Can cause compositor crashes. Consider this implementation of the Wlroots DataControl protocol to be unstable.


### Upcoming
* Any feature you request for... :)
