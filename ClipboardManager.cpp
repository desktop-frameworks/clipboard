/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * The DFL::Clipboard::Manager class manages the wayland clipboard.
 **/

#include "DFClipboardManager.hpp"
#include "Table.hpp"

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/DataControl.hpp>


DFL::Clipboard::Manager::Manager( QString path ) : QObject() {
    /** Registry */
    WQt::Registry *registry = new WQt::Registry( WQt::Wayland::display() );

    registry->setup();

    /** DataControlManager */
    mDataMgr.reset( registry->dataControlManager() );

    /** DataControlDevice and its connections */
    mDataDev.reset( mDataMgr->getDataDevice( registry->waylandSeat() ) );

    /** Handle the Clipboard offer */
    connect( mDataDev.get(), &WQt::DataControlDevice::selectionOffered,        this, &DFL::Clipboard::Manager::handleClipboardOffer );

    /** Handle the Primary Selection offer */
    connect( mDataDev.get(), &WQt::DataControlDevice::primarySelectionOffered, this, &DFL::Clipboard::Manager::handleSelectionOffer );

    /** Finish the mDataDev setup */
    mDataDev->setup();

    /* Clipboard History */
    clipboard = QSharedPointer<DFL::Clipboard::Table>::create( path, DFL::Clipboard::Clipboard );

    /* Selection History */
    selection = QSharedPointer<DFL::Clipboard::Table>::create( path, DFL::Clipboard::Selection );
}


void DFL::Clipboard::Manager::requestOffer( DFL::Clipboard::Type type, int idx ) {
    /**
     * Even though we can deny a request, at the moment, we don't have any reason to.
     * We honour all the requests for now.
     */

    qDebug() << "Requested" << ( type == DFL::Clipboard::Clipboard ? "Clipboard" : "Selection" ) << "at index" << idx;

    /**
     * Special request: Set the clipboard/selection to null.
     */
    if ( idx == -1 ) {
        if ( type == DFL::Clipboard::Clipboard ) {
            mDataDev->setSelection( nullptr );
        }

        else {
            mDataDev->setPrimarySelection( nullptr );
        }
    }

    switch ( type ) {
        case DFL::Clipboard::Clipboard: {
            clipboard->promoteToTop( idx );
            WQt::MimeData mimeData = clipboard->entry( 0 )->data();
            offerClipboard( mimeData );
            emit updateView( 0 );
            break;
        }

        case DFL::Clipboard::Selection: {
            selection->promoteToTop( idx );
            WQt::MimeData mimeData = selection->entry( 0 )->data();
            offerSelection( mimeData );
            emit updateView( 1 );
            break;
        }
    }
}


bool DFL::Clipboard::Manager::isSelectionEnabled() {
    /** If @mDisableSelections is true, selections are disabled */
    return not mDisableSelections;
}


void DFL::Clipboard::Manager::setSelectionDisabled( bool yes ) {
    mDisableSelections = yes;
}


bool DFL::Clipboard::Manager::isSelectionIgnored() {
    return mIgnoreSelections;
}


void DFL::Clipboard::Manager::setSelectionIgnored( bool yes ) {
    mIgnoreSelections = yes;
}


bool DFL::Clipboard::Manager::isHistoryStored() {
    return mStoreHistory;
}


void DFL::Clipboard::Manager::setStoreHistory( bool yes ) {
    mStoreHistory = yes;
    clipboard->setStoreHistory( yes );
    selection->setStoreHistory( yes );
}


quint64 DFL::Clipboard::Manager::historySize() {
    return mHistorySize;
}


void DFL::Clipboard::Manager::setHistorySize( quint64 size ) {
    mHistorySize = size;
    clipboard->setStoreHistory( size );
    selection->setStoreHistory( size );
}


bool DFL::Clipboard::Manager::isSizeLimitSet() {
    return mLimitSize;
}


quint64 DFL::Clipboard::Manager::largeDataSizeLimit() {
    /** If @mDisableSelections */
    return mMaxDataSize;
}


void DFL::Clipboard::Manager::setLargeDataSizeLimit( quint64 size ) {
    mLimitSize   = ( size == 0 ? false : true );
    mMaxDataSize = size;
}


void DFL::Clipboard::Manager::handleClipboardOffer( WQt::DataControlOffer *offer ) {
    /** This is our own offer. Nothing needs to be done about this. */
    if ( offer->offeredMimeTypes().contains( "application/x-DFL-clipboard" ) ) {
        /**
         * Leave in peace
         * This is wayland providing our own data back to us,
         * because, we're subscribed to all it's events.
         */

        return;
    }

    /** Alien data offer. Gather it. */
    WQt::MimeData mimeData = gatherOfferedData( offer );

    /**
     * Empty clipboard prevention:
     * Copy the mimeData to lastClipboard only if it was not empty.
     * Or if it was not large data.
     */
    if ( mimeData.formats().count() ) {
        if ( mimeData.formats().contains( "application/x-large-data" ) ) {
            int  size = mimeData.data( "application/x-large-data" ).toULongLong();
            emit largeData( size );

            return;
        }

        lastClipboard = mimeData;
    }

    clipboard->addEntry( lastClipboard );
    emit updateView( 0 );

    offerClipboard( lastClipboard );
}


void DFL::Clipboard::Manager::handleSelectionOffer( WQt::DataControlOffer *offer ) {
    /**
     * Ignore Clipboard
     * We will not re-offer the Primary Selection data.
     * We will ignore the data offer and return immediately.
     */
    if ( mIgnoreSelections ) {
        return;
    }

    /** This is our own offer. Nothing needs to be done about this. */
    if ( offer->offeredMimeTypes().contains( "application/x-DFL-clipboard" ) ) {
        /**
         * Leave in peace
         * This is wayland providing our own data back to us,
         * because, we're subscribed to all it's events.
         */

        return;
    }

    /**
     * If we are restoring the session and (Primary)Selections were disabled in the last
     * Session, we will again Disable Selections.
     */
    if ( mDisableSelections ) {
        /** Destroy the old data source */
        mDataDev->setPrimarySelection( nullptr );

        /** Return without gathering data or anything else */
        return;
    }

    /** Alien data offer. Gather it. */
    WQt::MimeData mimeData = gatherOfferedData( offer );

    /**
     * Empty clipboard prevention:
     * Copy the mimeData to lastSelection only if it was not empty.
     * Or if it was not large data.
     */
    if ( mimeData.formats().count() ) {
        if ( mimeData.formats().contains( "application/x-large-data" ) ) {
            int  size = mimeData.data( "application/x-large-data" ).toULongLong();
            emit largeData( size );

            return;
        }

        lastSelection = mimeData;
    }

    selection->addEntry( lastSelection );
    emit updateView( 1 );

    offerSelection( lastSelection );
}


void DFL::Clipboard::Manager::offerClipboard( WQt::MimeData mimeData ) {
    /**
     * This is the offer part of the Manager. Once the data is gathered,
     * It will be sent here for offering it. If the mimeData collected is null,
     * we will offer the last known good data.
     *
     * Applicable to: Clipboard (Wlroots::Selection)
     *
     * Prevent Empty Clipboard:
     * Apps like firefox are sensitive and sometimes can cause clipboard problems.
     * Similarly, if an app providing the data closes/crashes, the user will be
     * left with an empty clipboard. To prevent such cases, we will re-offer the
     * data we've received.
     *
     * BUG: Currently, there is no way to disable this "feature"
     */

    if ( mimeData.formats().count() ) {
        if ( not ( lastClipboard == mimeData ) ) {
            lastClipboard = mimeData;
        }
    }

    /** Destroy the old data source */
    mDataDev->setSelection( nullptr );

    /** Create a new data source, and set the mimeData */
    clpSrc.reset( mDataMgr->createDataSource() );

    clpSrc->setSelectionData( lastClipboard );

    /** Offer the mimeTypes */
    clpSrc->offer( "application/x-DFL-clipboard" );
    for ( QString fmt: mimeData.formats() ) {
        clpSrc->offer( fmt );
    }

    /** Set this source as the new selection to the data_device */
    mDataDev->setSelection( clpSrc.get() );
}


void DFL::Clipboard::Manager::offerSelection( WQt::MimeData mimeData ) {
    /**
     * This is the offer part of the Manager. Once the data is gathered,
     * It will be sent here for offering it. If the mimeData collected is null,
     * we will offer the last known good data.
     *
     * Applicable to: Selection (Wlroots::PrimarySelection)
     *
     * Prevent Empty Clipboard:
     * Apps like firefox are sensitive and sometimes can cause clipboard problems.
     * Similarly, if an app providing the data closes/crashes, the user will be
     * left with an empty clipboard. To prevent such cases, we will re-offer the
     * data we've received.
     *
     * BUG: Currently, there is no way to disable this "feature"
     */

    if ( mimeData.formats().count() ) {
        if ( lastSelection != mimeData ) {
            lastSelection = mimeData;
        }
    }

    /** Destroy the old data source */
    mDataDev->setPrimarySelection( nullptr );

    /** Create a new data source, and set the mimeData */
    selSrc.reset( mDataMgr->createDataSource() );

    selSrc->setSelectionData( lastSelection );

    /** Offer the mimeTypes */
    selSrc->offer( "application/x-DFL-clipboard" );
    for ( QString fmt: mimeData.formats() ) {
        selSrc->offer( fmt );
    }

    /** Set this source as the new selection to the data_device */
    mDataDev->setPrimarySelection( selSrc.get() );
}


WQt::MimeData DFL::Clipboard::Manager::gatherOfferedData( WQt::DataControlOffer *offer ) {
    /**
     * Since we provide the "Prevent Empty Clipboard feature", we have an associated problem.
     * Some apps like image-editors can overload the clipboard with large data.
     * For example:
     * Copying an image with gimp will result in a copy buffer of 100+ MiB data per format,
     * with multiple formats made available: image/bmp, image/jpeg, image/png, image/tiff,
     * image/x-MS-bmp, image/x-bmp. Out of these, image/bmp, image/x-MS-bmp and image/x-bmp
     * are the same format and can be very large (several tens of MiB to even 100+ MiB).
     *
     * These situations can cause memory problems. To prevent such issues, we're giving the
     * option to the user to limit the memory usage. In case the size of the data exceeds a
     * particular limit, then, mimeData will contain only one format `application/x-large-data`,
     * and the data will be the size of the data received.
     * When the handlers receive this format, they will not try to store/re-offer the data.
     */

    WQt::MimeData mimeData;
    quint64       dataSize = 0;

    if ( offer && offer->isValid() ) {
        QStringList mimeTypes( offer->offeredMimeTypes() );
        qDebug() << mimeTypes << "were offered";
        QByteArray previous;
        for ( QString mt: mimeTypes ) {
            if ( not offer->isValid() ) {
                qDebug() << "Invalidated offer";
            }

            qDebug() << "Asking for" << mt;
            QByteArray data = offer->retrieveData( mt );

            /** We have some valid data */
            if ( data.length() ) {
                mimeData.setData( mt, data );
                previous = data;
                qDebug() << data;
            }

            /** We have previously retrieved data */
            else if ( previous.length() ) {
                mimeData.setData( mt, data );
                previous = data;
            }

            /** Count the size of the data in the mimeData object */
            dataSize += previous.length();
        }

        QObject::connect(
            offer, &WQt::DataControlOffer::invalidated, [ = ] () {
                qDebug() << "Disconnected";
                offer->disconnect();
            }
        );
    }

    if ( mLimitSize ) {
        if ( dataSize > mMaxDataSize ) {
            mimeData.clear();
            mimeData.setData( "application/x-large-data", QByteArray::number( dataSize ) );
        }
    }

    return mimeData;
}
